import * as Eficy from '@eficy/core';

import './index.scss';

export default function Layout(props: {
  pages: Array<{ path: string; name: string; model: any; icon?: string }>;
}) {
  const { pages } = props;
  const pageMap = pages.reduce(
    (previousValue, currentValue) =>
      Object.assign(previousValue, { [currentValue.path]: currentValue }),
    {}
  );
  const currentPage = pages[0];
  return Eficy.createElement({
    id: 'layout',
    className: 'layout-container',
    '#view': 'Layout',
    '#children': [
      {
        '#view': 'Layout.Sider',
        '#children': [
          {
            '#view': 'div',
            className: 'logo'
          },
          {
            '#view': 'Menu',
            '@onClick': (ctrl, { key }) => {
              const gotPage = pageMap[key];
              if (!gotPage) {
                return;
              }
              ctrl.models.pageContent.controller.run({
                action: 'reload',
                data: gotPage.model
              });
            },
            '#children': [
              ...pages.map(pageInfo => ({
                '#view': 'Menu.Item',
                '#children': [
                  {
                    '#view': `Icons.${pageInfo.icon}`
                  },
                  {
                    '#view': 'span',
                    '#content': pageInfo.name
                  }
                ],
                key: pageInfo.path
              }))
            ],
            theme: 'dark',
            mode: 'inline',
            defaultSelectedKeys: [currentPage.path]
          }
        ],
        trigger: null
      },
      {
        '#view': 'Layout',
        '#children': [
          {
            '#view': 'Layout.Header',
            '#children': [
              {
                '#view': 'Icon',
                className: 'trigger'
              }
            ],
            style: {
              background: '#fff',
              padding: 0
            }
          },
          {
            '#view': 'Layout.Content',
            style: {
              margin: '24px 16px',
              padding: 24,
              background: '#fff',
              minHeight: 'calc(100vh - 64px - 48px)'
            },
            '#children': [
              {
                '#': 'pageContent',
                '#view': 'Eficy',
                ...currentPage.model
              }
            ]
          }
        ]
      }
    ]
  });
}
