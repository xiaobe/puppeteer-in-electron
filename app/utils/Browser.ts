/**
 * Created by yee.wang on 2019-01-31
 **/

import pie from 'puppeteer-in-electron';
import * as puppeteer from 'puppeteer-core';

export default class Browser {
  private static openedMap = new Map();

  public static async getPuppeteer() {
    const { BrowserWindow, app } = require('electron').remote;
    const browser = await pie.connect(app, puppeteer);

    const window = new BrowserWindow({
      width: 400,
      height: 700
    });

    const page = pie.getPage(browser, window);

    this.openedMap.set(page, { browser, window });

    return page;
  }

  public static async close(page) {
    if (page) {
      const { browser, window } = this.openedMap.get(page);
      browser.close();
      window.destroy();
    }
  }
}
