import React, { Fragment } from 'react';
import { render } from 'react-dom';
import { AppContainer as ReactHotAppContainer } from 'react-hot-loader';
import * as Eficy from '@eficy/core';
import * as antd from 'antd';
import * as icons from '@ant-design/icons';
import './app.global.css';
import Layout from './components/Layout';
import home from './pages/home/home';

Eficy.Config.successAlert = ({ msg }) => antd.message.success(msg);
Eficy.Config.failAlert = ({ msg }) => antd.message.error(msg);
Eficy.Config.defaultComponentMap = { ...antd, Icons: icons };

const AppContainer = process.env.PLAIN_HMR ? Fragment : ReactHotAppContainer;

document.addEventListener('DOMContentLoaded', () => {
  const pages = [
    {
      path: '/',
      name: 'Home',
      icon: 'HomeOutlined',
      model: home
    },
    {
      path: '/2',
      name: 'Home2',
      icon: 'HomeOutlined',
      model: {
        views: [
          {
            '#view': 'div',
            '#content': 'Page 2'
          }
        ]
      }
    }
  ];
  render(
    <AppContainer>
      <Layout pages={pages} />
    </AppContainer>,
    document.getElementById('root')
  );
});
