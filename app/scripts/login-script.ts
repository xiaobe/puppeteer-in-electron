import BaseScript from './base-script';

export const LOGIN_URL = 'https://login.taobao.com/member/login.jhtml';

export default class LoginScript extends BaseScript {
  protected page: any;
  protected cookies: any;
  protected close?: () => void;

  async run() {
    await this.login();
  }

  protected async login() {
    const { page } = this;

    await page.deleteCookie(...(await page.cookies(LOGIN_URL)));

    await page.goto(LOGIN_URL);
    await page.waitFor('.login-title');

    await page.type('input[name="TPL_username"]', 'q515303600');
    await page.type('input[name="TPL_password"]', 'xxxxxxxxxx', { delay: 100 });

    await page.click('button#J_SubmitStatic');

    await page.waitFor(() => {
      return !!document.querySelector('.site-nav-login-info-nick');
    });

    this.cookies = await page.cookies();

    console.log('got cookies ', this.cookies);
  }
}
