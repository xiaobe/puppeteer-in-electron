import Browser from '../utils/Browser';

interface IBaseScript {
  page?: any;
}

export default class BaseScript {
  static run({ page }: IBaseScript = {} as IBaseScript) {
    const script = new BaseScript({ page });

    return script.run();
  }

  protected page: any;
  protected close?: () => void;

  constructor({ page }: IBaseScript = {} as IBaseScript) {
    this.page = page;
  }

  async run() {
    // Need to be derived
  }

  protected async _run() {
    if (!this.page) {
      this.page = await Browser.getPuppeteer();
      this.close = () => Browser.close(this.page);
    }

    await this.run();

    this.close && this.close();
  }
}
