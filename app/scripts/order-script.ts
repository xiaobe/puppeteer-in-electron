import BaseScript from './base-script';
import LoginScript from './login-script';

export default class OrderScript extends BaseScript {
  async run() {
    await LoginScript.run({ page: this.page });

    // some script
  }
}
